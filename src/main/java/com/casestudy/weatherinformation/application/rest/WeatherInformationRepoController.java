package com.casestudy.weatherinformation.application.rest;

import com.casestudy.weatherinformation.application.dto.WeatherInformationDTO;
import com.casestudy.weatherinformation.metier.bean.WeatherInformationBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Map;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/weather")
public class WeatherInformationRepoController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/information", produces = {"application/json"})
    public ResponseEntity<WeatherInformationDTO> getWeatherInformation(@RequestParam("lat") String latitude,
                                                                       @RequestParam("long") String longitude){

        Map<String, Object> response = restTemplate.getForObject(
                "http://api.geonames.org/findNearByWeatherJSON?lat=" + latitude + "&lng=" + longitude + "&username=aerramach",
                Map.class);
        System.out.println(response);
        Map<String, Object> weatherObservation = (Map<String, Object>) response.get("weatherObservation");

        WeatherInformationBean weatherInformationBean = WeatherInformationBean.builder()
                .humidity((int) weatherObservation.get("humidity"))
                .temperature((String) weatherObservation.get("temperature"))
                .windSpeed((String) weatherObservation.get("windSpeed"))
                .stationName((String) weatherObservation.get("stationName"))
                .build();

        return new ResponseEntity<>(WeatherInformationDTO.toWeatherInformationDTO(weatherInformationBean), HttpStatus.OK);
    }
}
