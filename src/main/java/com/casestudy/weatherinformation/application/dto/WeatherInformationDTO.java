package com.casestudy.weatherinformation.application.dto;


import com.casestudy.weatherinformation.metier.bean.WeatherInformationBean;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Optional;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access = PRIVATE)
@SuperBuilder
@ToString
public class WeatherInformationDTO {

    private String windSpeed;
    private String temperature;
    private int humidity;
    private String stationName;

    public static WeatherInformationDTO toWeatherInformationDTO(WeatherInformationBean weatherInformationBean) {
        return Optional.ofNullable(weatherInformationBean).map(nonNull -> WeatherInformationDTO.builder()
                        .humidity(nonNull.getHumidity())
                        .temperature(nonNull.getTemperature())
                        .windSpeed(nonNull.getWindSpeed())
                        .stationName(nonNull.getStationName())
                        .build())
                .orElse(null);
    }


}
