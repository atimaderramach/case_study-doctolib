package com.casestudy.weatherinformation.metier.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeatherInformationBean {

    private String windSpeed;

    private String temperature;

    private int humidity;

    private String stationName;

}
