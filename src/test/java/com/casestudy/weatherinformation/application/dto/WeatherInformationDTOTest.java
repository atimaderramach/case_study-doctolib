package com.casestudy.weatherinformation.application.dto;

import com.casestudy.weatherinformation.metier.bean.WeatherInformationBean;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class WeatherInformationDTOTest {

    @Test
    void getWeatherTest(){
        WeatherInformationBean weatherInformationBean= WeatherInformationBean.builder()
                .humidity(12)
                .temperature("144")
                .windSpeed("15")
                .stationName("Hong Kong")
                .build();

        WeatherInformationDTO weatherInformationDTOexpected= WeatherInformationDTO.builder()
                .humidity(12)
                .temperature("144")
                .windSpeed("15")
                .stationName("Hong Kong")
                .build();

        WeatherInformationDTO weatherInformationDTO = WeatherInformationDTO.toWeatherInformationDTO(weatherInformationBean);

        assertEquals(weatherInformationDTO.getClass(), weatherInformationDTOexpected.getClass());
        assertEquals(weatherInformationDTO.getHumidity(), weatherInformationDTOexpected.getHumidity());
        assertEquals(weatherInformationDTO.getWindSpeed(), weatherInformationDTOexpected.getWindSpeed());
        assertEquals(weatherInformationDTO.getTemperature(), weatherInformationDTOexpected.getTemperature());


    }



}
