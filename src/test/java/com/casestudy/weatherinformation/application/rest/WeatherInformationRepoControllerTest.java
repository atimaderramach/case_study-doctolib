package com.casestudy.weatherinformation.application.rest;

import com.casestudy.weatherinformation.application.dto.WeatherInformationDTO;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class WeatherInformationRepoControllerTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private WeatherInformationRepoController weatherInformationRepoController;


    @Test
    void getWeatherInformationTest() {
        Map<String, Object> map = new HashMap<>()
        {{
            put("elevation", 8);
            put("lng", 9.8);
            put("observation", "FGBT 180500Z 00000KT 9999 SCT008 23/23 Q1009");
        }};
        //"[{elevation=8, lng=9.8, observation=FGBT 180500Z 00000KT 9999 SCT008 23/23 Q1009, ICAO=FGBT, clouds=scattered clouds, dewPoint=23, cloudsCode=SCT, datetime=2022-05-18 05:00:00, countryCode=GQ, temperature=23, humidity=100, stationName=BATA (RIO MUNI), weatherCondition=n/a, windDirection=0, hectoPascAltimeter=1009, windSpeed=00, lat=1.8833333333333333}]"



        //map.put("weatherObservation",object);
        WeatherInformationDTO weatherInformationExpected = WeatherInformationDTO.builder().humidity(100).windSpeed("00").temperature("24").build();

        Mockito.when(restTemplate.getForObject("http://api.geonames.org/findNearByWeatherJSON?lat=2&lng=10&username=aerramach", Map.class)).thenReturn(map);

        final var informationDTO = weatherInformationRepoController.getWeatherInformation("2","10");

        assertEquals(informationDTO, weatherInformationExpected);

    }
}
